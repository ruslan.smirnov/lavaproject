# LavaProject

Unity Developer C# Lava Project (LAVA GAMES)

ТЗ Developer

Краткое описание:
Человек добегает до зоны прыжка и прыгает назад, делая заднее сальто, приземляясь в точку касания.

Уточнения:
* игра из простых 3д форм
* Брусок символизирующий человечка бежит по направлению к диску, лежащему на земле.
* диск обозначает зону отталкивания
* если тапнуть на землю позади человека в момент, когда человек будет находиться на диске, он прыгает в точку касания, делая полное сальто в воздухе за время прыжка.
* после приземления человек начинает бежать к диску с помощью Scriptable objects задать три разные настройки прыжка, которые будут отличаться высотой прыжка и размером диска (зоны отталкивания)
* если игрок не успевает нажать на прыжок, человек добегая до конца экрана появляется на начальной позиции и продолжает бежать к диску
* После удачного прыжка настройки высоты прыжка и размера диска меняются на следующие и идут по кругу (1 -> 2 -> 3 -> 1 -> 2…)
* Сверху необходимо отображать счетчик количества удачных прыжков.
* количество удачных прыжков нужно сохранять и начинать отсчет с этого числа при перезагрузке игры
* сохранение нужно сделать наиболее приемлимым на ваш взгляд способом

Примечания:
* Для упрощения можно использовать только оси X и Y для движения, чтобы все находилось в одной плоскости
* Вращение в воздухе предпочтительно сделать через Аниматор и триггер.
* Траекторию полета предпочтительно сделать без использования физических сил