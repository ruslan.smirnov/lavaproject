﻿using UnityEngine;
using TSP.LavaProject.Core;

public sealed class EntryPoint : MonoBehaviour
{
    private SceneLoader _sceneLoader = null;
    private GameManager _gameManager = null;

    private void Awake()
    {
        _sceneLoader = new SceneLoader();
        _gameManager = new GameManager();
    }

    private void Start()
    {
        _sceneLoader.Setup();
        _gameManager.Setup();

        _sceneLoader.LoadScene(SceneID.Game);
    }

    private void Update()
    {
        _sceneLoader.Update();
        _gameManager.Update();
    }
}
