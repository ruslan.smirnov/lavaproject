﻿using UnityEngine;
using UnityEngine.EventSystems;
using TSP.LavaProject.Core;

namespace TSP.LavaProject.Game
{
    public sealed class TouchReceiver : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            //TO DO: OnPointerUp не работает без OnPointerDown
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            GameManager.Instance.OnTouchScreen(eventData);
        }
    }
}
