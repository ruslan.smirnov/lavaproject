﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSP.LavaProject.Game
{
    public sealed class AnimationEventsReceiver : MonoBehaviour
    {
        public Action<string> EventReceived;

        public void OnReceiveAnimationEvent(string eventName)
        {
            EventReceived?.Invoke(eventName);
        }
    }
}

