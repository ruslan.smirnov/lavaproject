﻿using UnityEngine;
using TSP.LavaProject.Core;

// first https://gamedev.stackexchange.com/questions/108201/movement-issues-jumping-to-another-gameobject

/*

//  global

    private Vector3 _targetPosition;
    private Vector3 _velocity = Vector3.zero;
    private Vector3 _gravity = 9.8f * Vector3.down;

//  init 

    float timeToJump = 1;
    var direction = _targetPosition - transform.position;
    _velocity = (direction - (Mathf.Pow(timeToJump, 2) * 0.5f * _gravity)) / timeToJump;

//  update

    var delta = Time.deltaTime;
    _velocity += delta * _gravity;
    transform.position += delta * _velocity;

*/

// second (using) https://answers.unity.com/questions/8318/throwing-object-with-acceleration-equationscript.html

namespace TSP.LavaProject.Game
{
    public enum UnityChanState
    {
        Idle = 0,
        Run = 1,
        StartJump = 2,
        Jump = 3,
    }

    public sealed class Player : MonoBehaviour
    {
        [SerializeField]
        private Animator _rootAnimator = null;

        [SerializeField]
        private Animator _charactorAnimator = null;

        [SerializeField]
        private AnimationEventsReceiver _eventsReceiver = null;

        [SerializeField]
        private Rigidbody _rigidbody = null;

        private UnityChanState _state = UnityChanState.Idle;

        private JumpCircle _jumpCircle = null;
        private GameStageInfo _stageInfo = default;

        private Vector3 _awakePosition = default;
        private Vector3 _targetPosition = default;

        private Vector3 _jumpStartPosition = default;
        private Vector3 _jumpFinalPosition = default;
        private float _jumpIncrementor = default;
        private float _jumpTime = default;

        public void HandleAnimationEvent(string eventName)
        {
            switch (eventName)
            {
                case GameManager.IS_JUMP_ANIMATION_EVENT:

                    _jumpStartPosition = transform.position;
                    _jumpFinalPosition = _targetPosition;
                    _jumpIncrementor = 0;
                    _jumpTime = _stageInfo.JumpTime;

                    _rigidbody.useGravity = false;
                    _rootAnimator.SetTrigger(GameManager.IS_JUMP_ANIMATOR_TRIGGER);
                    _state = UnityChanState.Jump;

                    break;
                default:
                    break;
            }
        }

        public bool TryRun()
        {
            if (_state != UnityChanState.Idle)
            {
                return false;
            }

            GameManager gm = GameManager.Instance;

            _stageInfo = gm.Stageinfo;
            _jumpCircle = gm.JumpCircle;
            _state = UnityChanState.Run;
            _charactorAnimator.SetTrigger(GameManager.IS_RUN_ANIMATOR_TRIGGER);

            return true;
        }

        public bool TryJump(Vector3 position)
        {
            if (_state != UnityChanState.Run)
            {
                return false;
            }

            var target = new Vector2(position.x, position.z);
            var current = new Vector2(transform.position.x, transform.position.z);
            var direction = target - current;
            var angle = Vector2.Dot(direction, Vector2.down);

            if (angle > 0)
            {
                return false;
            }

            _targetPosition = position;
            _charactorAnimator.SetTrigger(GameManager.IS_JUMP_ANIMATOR_TRIGGER);
            _state = UnityChanState.StartJump;

            return true;
        }

        public void Reset()
        {
            transform.position = _awakePosition;
            _state = UnityChanState.Idle;

            TryRun();
        }

        private void Awake()
        {
            GameManager.Instance.SetUnityChan(this);

            _awakePosition = transform.position;
        }

        private void Start()
        {
            _eventsReceiver.EventReceived += HandleAnimationEvent;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == GameManager.Instance.JumpCircle.gameObject)
            {
                Reset();
            }
        }

        private void Update()
        {
            if (_state == UnityChanState.Run)
            {
                Vector3 targetPosition = _jumpCircle.transform.position;

                float distance = Vector2.Distance(
                    new Vector2(targetPosition.x, targetPosition.z),
                    new Vector2(transform.position.x, transform.position.z));

                if (distance > 0.1f)
                {
                    var position = Vector3.MoveTowards(
                        transform.position,
                        targetPosition,
                        _stageInfo.MoveSpeed * Time.deltaTime);

                    _rigidbody.MovePosition(position);
                }
            }

            if (_state == UnityChanState.Jump)
            {
                float distance = Vector3.Distance(_targetPosition, transform.position);

                if (distance > 0.1f)
                {
                    _jumpIncrementor += Time.deltaTime / _jumpTime;

                    var currentPos = Vector3.Lerp(_jumpStartPosition, _jumpFinalPosition, _jumpIncrementor);
                    currentPos.y += _stageInfo.JumpHeight * Mathf.Sin(Mathf.Clamp01(_jumpIncrementor) * Mathf.PI);
                    transform.position = currentPos;
                }
                else
                {
                    _rigidbody.useGravity = true;
                    _state = UnityChanState.Idle;
                    
                    GameManager.Instance.OnPlayerJumpCompleted();

                    TryRun();
                }
            }
        }
    }
}

