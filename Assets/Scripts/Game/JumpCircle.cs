﻿using TSP.LavaProject.Core;
using UnityEngine;

namespace TSP.LavaProject.Game
{
    public sealed class JumpCircle : MonoBehaviour
    {
        public void OnChangeStage()
        {
            var radius = GameManager.Instance.Stageinfo.RadiusCircle;
            transform.localScale = new Vector3(radius, 0, radius);
        }

        private void Awake()
        {
            GameManager.Instance.SetJumpCircle(this);
        }

        private void Start()
        {
            OnChangeStage();
        }
    }
}

