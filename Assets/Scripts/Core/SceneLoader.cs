﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

namespace TSP.LavaProject.Core
{
    public enum SceneID
    {
        None = -1,
        EntryPoint = 0,
        Game = 1,
    }

    public sealed class SceneLoader : DataManager<SceneLoader>
    {
        public Action SceneLoaded;

        private SceneID _targetScene = SceneID.None;

        public SceneLoader()
        {

        }

        private void UnloadCurrent()
        {
            SceneManager.sceneUnloaded += OnCurrentSceneUnloaded;
            SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        }

        private void OnCurrentSceneUnloaded(Scene scene)
        {
            SceneManager.sceneUnloaded -= OnCurrentSceneUnloaded;

            SceneManager.sceneLoaded += OnSceneGameLoaded;
            SceneManager.LoadSceneAsync(_targetScene.ToString(), LoadSceneMode.Additive);
        }

        private void OnSceneGameLoaded(Scene scene, LoadSceneMode loadMode)
        {
            SceneManager.sceneLoaded -= OnSceneGameLoaded;
            SceneManager.SetActiveScene(scene);

            _targetScene = SceneID.None;
            SceneLoaded?.Invoke();
        }

        public void LoadScene(SceneID sceneID)
        {
            string initSceneName = SceneID.EntryPoint.ToString();
            string targetSceneName = sceneID.ToString();
            string currentSceneName = SceneManager.GetActiveScene().name;

            if (currentSceneName == initSceneName)
            {
                // special case: first load game
                SceneManager.sceneLoaded += OnSceneGameLoaded;
                SceneManager.LoadSceneAsync(targetSceneName, LoadSceneMode.Additive);
            }
            else if (targetSceneName == initSceneName)
            {
                // special case
            }
            else
            {
                _targetScene = sceneID;
                UnloadCurrent();
            }
        }

        public void Setup()
        {

        }

        public void Update()
        {

        }
    }
}
