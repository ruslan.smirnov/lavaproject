﻿using UnityEngine;
using UnityEngine.UI;

namespace TSP.LavaProject.Core
{
    public sealed class GameViewer : MonoBehaviour
    {
        [SerializeField]
        private Text _jumpCounterText = null;

        [SerializeField]
        private Button _runButton = null;

        public void SetActiveRunButton(bool value)
        {
            _runButton.gameObject.SetActive(value);
        }

        public void OnChangeumpCounter()
        {
            _jumpCounterText.text = string.Format(
                GameManager.JUMP_COUNTER_FORMAT, 
                GameManager.Instance.Score.ToString());
        }

        private void Awake()
        {
            GameManager.Instance.SetViewer(this);

            _runButton.onClick.AddListener(GameManager.Instance.OnClickRunButton);
        }

        private void Start()
        {
            OnChangeumpCounter();
        }

        private void OnDestroy()
        {
            _runButton.onClick.RemoveAllListeners();
        }
    }
}

