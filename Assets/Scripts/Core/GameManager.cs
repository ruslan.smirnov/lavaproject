﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TSP.LavaProject.Game;

namespace TSP.LavaProject.Core
{
    public sealed class GameManager : DataManager<GameManager>
    {
        public const string PLAYER_PREFS_SCORE = "score";
        public const string PATH_TO_GAME_CONFIG = "Configs/GameConfig";
        public const string GROUND_LAYER_NAME = "Ground";
        public const string JUMP_COUNTER_FORMAT = "Jump Counter: {0}";
        public const string IS_JUMP_ANIMATION_EVENT = "IsJump";
        public const string IS_JUMP_ANIMATOR_TRIGGER = "IsJump";
        public const string IS_RUN_ANIMATOR_TRIGGER = "IsRun";

        private int _currentStageIndex = 0;
        private readonly List<GameStageInfo> _stages = new List<GameStageInfo>();

        private GameViewer _viewer = null;

        public GameConfig GameConfig { get; private set; }
        public Player Player { get; private set; }
        public JumpCircle JumpCircle { get; private set; }
        public GameStageInfo Stageinfo { get { return _stages[_currentStageIndex]; } }
        public int Score { get { return PlayerPrefs.GetInt(PLAYER_PREFS_SCORE); } }

        public GameManager()
        {

        }

        private void IncScore()
        {
            int value = Score;
            PlayerPrefs.SetInt(PLAYER_PREFS_SCORE, ++value);
            _viewer.OnChangeumpCounter();
        }

        public void Setup()
        {
            GameConfig = Resources.Load<GameConfig>(PATH_TO_GAME_CONFIG);
            _stages.AddRange(GameConfig.GetStages());
        }

        public void SetUnityChan(Player player)
        {
            Player = player;
        }

        public void SetJumpCircle(JumpCircle jumpCircle)
        {
            JumpCircle = jumpCircle;
        }        

        public void SetViewer(GameViewer viewer)
        {
            _viewer = viewer;
        }

        public void OnTouchScreen(PointerEventData eventData)
        {
            Vector3 worldPoint = Camera.main.ScreenToWorldPoint(eventData.position);
            Ray ray = Camera.main.ScreenPointToRay(eventData.position);

            if (Physics.Raycast(ray, out RaycastHit hit, 100, LayerMask.GetMask(GROUND_LAYER_NAME)))
            {
                if (Player.TryJump(hit.point))
                {
                    IncScore();
                }
            }
        }

        public void OnClickRunButton()
        {
            if (Player.TryRun())
            {
                _viewer.SetActiveRunButton(false);
            }
        }

        public void OnPlayerJumpCompleted()
        {
            _currentStageIndex = _currentStageIndex + 1 < _stages.Count ? ++_currentStageIndex : 0;
            JumpCircle.OnChangeStage();
        }

        public void Update()
        {
        }
    }
}

