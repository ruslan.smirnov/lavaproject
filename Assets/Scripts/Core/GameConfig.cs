﻿using System.Collections.Generic;
using UnityEngine;

namespace TSP.LavaProject.Core
{
    [System.Serializable]
    public class GameStageInfo
    {
        [SerializeField]
        private float _radiusCircle = default;

        [SerializeField]
        private float _moveSpeed = default;

        [SerializeField]
        private float _jumpHeight = default;

        [SerializeField]
        private float _jumpTime = default;

        public float RadiusCircle { get { return _radiusCircle; } }
        public float MoveSpeed { get { return _moveSpeed; } }
        public float JumpHeight { get { return _jumpHeight; } }
        public float JumpTime { get { return _jumpTime; } }
    }

    [CreateAssetMenu(fileName = "GameConfig", menuName = "ScriptableObjects/GameConfig")]
    public sealed class GameConfig : ScriptableObject
    {
        [SerializeField]
        private List<GameStageInfo> _stages = new List<GameStageInfo>();

        public List<GameStageInfo> GetStages()
        {
            return new List<GameStageInfo>(_stages);
        }
    }
}

